exports = (typeof window === 'undefined') ? global : window;

exports.regexAnswers = {
  containsNumber : function(str) {
      // 'abc123' should return true
      //// 'abc' should return false
      var reg = /^(?=.*\d)(?=.*[a-zA-Z]).{4,8}$/;
      return reg.test(str);


  },

  containsRepeatingLetter : function(str) {
      // 'aabc' should return true
      // 'abc' should return false
      var reg = /(\\w)\\1+/;
      return reg.test(str);
  },

  endsWithVowel : function(str) {
      // 'something' should return false
      // 'create' should return true

      var reg = /^[aeiouy]+$/;
      return reg.test(str);
  },

  isUSD : function(str) {
      // for example:
      // '$132.03' should be true
      // '$132,219' should be true
      // '$132,212.43' should be true
      // '$132.034,233' should be false
      // '$132.034_23' should be false
      // '$132.013,14.23' should be false

      var r = /^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*\.[0-9]{2}$/;
      console.log(r.test(str));
      return(r.test(str))
  }
};
