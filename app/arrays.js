exports = (typeof window === 'undefined') ? global : window;

exports.arraysAnswers = {

  indexOf : function(arr, item) {
      var i,
          size = arr.length;
      for (i = 0; i < size; i += 1) {
          if (item === arr[i]) {
              return i;
          }
      }
      return -1;
  },

  sum : function(arr) {
      var i,
          size = arr.length,
          sum = 0;
      for (i = 0; i < size; i += 1) {
         sum += arr[i];
      }
      return sum;
  },

  remove : function(arr, item) {
      var i,
          size = arr.length,
          temp = arr;

      for (i = 0; i < size; i += 1) {
          if (item === temp[i]) {
              temp.splice(i, 1);
              i -= 1;
          }
      }
      return temp;
  },

  removeWithoutCopy : function(arr, item) {
      var i,
          size = arr.length;
        //console.log(item);
      //console.log(arr);
      for (i = 0; i < size; i += 1) {
          if (item === arr[i]) {
              //console.log(i);
              arr.splice(i, 1);
              i -= 1;
          }
      }
      //console.log(arr);
      return arr;
  },

  append : function(arr, item) {
      var size = arr.length;
      arr[size] = item;
      return arr;
  },

  truncate : function(arr) {
      var size = arr.length;
      //arr[size] = item;
      arr.splice(size-1, 1);
      return arr;
  },

  prepend : function(arr, item) {
      arr.splice(0, 0, item);
      return arr
  },

  curtail : function(arr) {
      arr.splice(0, 1);
      return arr
  },

  concat : function(arr1, arr2) {
      var c = arr1.concat(arr2);
      return c;
  },

  insert : function(arr, item, index) {
      //console.log(arr);
      arr.splice(index, 0, item);
      //console.log(arr);
  },

  count : function(arr, item) {
      var i,
          size = arr.length,
        count = 0;
      for (i = 0; i < size; i += 1) {
          if (item === arr[i]) {
              count += 1;
          }
      }
      return count;
  },

  duplicates : function(arr) {
      var i,
          size = arr.length,
          aux = arr[0],
          duplex = [];
      for (i = 0; i < size; i += 1) {
          if (aux === arr[i]) {
              if (duplex.indexOf(aux) == -1){
                  duplex.push(aux);
              }

          } else {
              aux = arr[i];
          }
      }
      return duplex;
  },

  square : function(arr) {
      var i,
          size = arr.length;
      for (i = 0; i < size; i += 1) {
          arr[i] = Math.pow(arr[i],2);
      }
      return arr;
  },

  findAllOccurrences : function(arr, target) {
      var i,
          size = arr.length,
          count = 0,
          ocuur = [];

      for (i = 0; i < size; i += 1) {
          if (target === arr[i]) {
              ocuur.push(i);
          }
      }
      return ocuur;
  }
};
