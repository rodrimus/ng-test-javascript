if ( typeof window === 'undefined' ) {
  require('../../app/arrays');
  var expect = require('chai').expect;
}

describe('arrays', function() {
  var a;

  beforeEach(function() {
    a = [ 1, 2, 3, 4 ];
  });

  it('#indexOf(arr,item) you should be able to determine the location of an item in an array Example: indexOf([1,2,3,4,5],3) should be 2', function() {
    expect(arraysAnswers.indexOf(a, 3)).to.eql(2);
    expect(arraysAnswers.indexOf(a, 5)).to.eql(-1);
  });

  it('#sum(arr) should be able to add the values of an array. Example: sum([1,2,3,4])  should be 10', function() {
    expect(arraysAnswers.sum(a)).to.eql(10);
  });

  it('#remove(arr,item) you should be able to remove all instances of a value from an array. Example: remove([1,2,3,4],2) should be [1,3,4]', function() {
    a.push(2); // Make sure the value appears more than one time
    var result = arraysAnswers.remove(a, 2);

    expect(result).to.have.length(3);
    expect(result.join(' ')).to.eql('1 3 4');
  });

  it('#removeWithoutCopy(arr,item) you should be able to remove all instances of a value from an array, returning the original array Example: removeWithoutCopy([1,2,3,4],2) should be [1,3,4](the same array instance)', function() {
    a.splice( 1, 0, 2 );
    a.push( 2 );
    a.push( 2 );

    var result = arraysAnswers.removeWithoutCopy(a, 2);

    expect(result).to.have.length(3);
    expect(result.join(' ')).to.eql('1 3 4');

    // make sure that you return the same array instance
    expect(result).equal(a);
  });

  it('#append(arr,item) you should be able to add an item to the end of an array. Example: append([1,2,3,4],5) should be [1,2,3,4,5]', function() {
    var result = arraysAnswers.append(a, 10);

    expect(result).to.have.length(5);
    expect(result[result.length - 1]).to.eql(10);
  });

  it('#truncate(arr,item) you should be able to remove the last item of an array. Example: truncate([1,2,3,4]) should be [1,2,3]', function() {
    var result = arraysAnswers.truncate(a);

    expect(result).to.have.length(3);
    expect(result.join(' ')).to.eql('1 2 3');
  });

  it('#prepend(arr,item) you should be able to add an item to the beginning of an array. Example: prepend([1,2,3,4],5) should be [5,1,2,3,4]', function () {
    var result = arraysAnswers.prepend(a, 10);

    expect(result).to.have.length(5);
    expect(result[0]).to.eql(10);
  });

  it('#curtail(arr) you should be able to remove the first item of an array. Example: curtail([1,2,3,4]) should be [2,3,4]', function () {
    var result = arraysAnswers.curtail(a);

    expect(result).to.have.length(3);
    expect(result.join(' ')).to.eql('2 3 4');
  });

  it('#concat(arr1,arr2) you should be able to join together two arrays. Example: concat([1,2,3],[a,b,c]) should be [1,2,3,a,b,c]', function() {
    var c = [ 'a', 'b', 'c', 1 ];
    var result = arraysAnswers.concat(a, c);

    expect(result).to.have.length(8);
    expect(result.join(' ')).to.eql('1 2 3 4 a b c 1');
  });

  it('#insert(arr,item,index) you should be able to add an item anywhere in an array. Example: insert([1,2,3,4,5],z,2) should be [1,2,z,3,4,5]', function() {
    var result = arraysAnswers.insert(a, 'z', 2);

    expect(result).to.have.length(5);
    expect(result.join(' ')).to.eql('1 2 z 3 4');
  });

  it('#count(arr,item) you should be able to count the occurences of an item in an array. Example: count([1,2,1,3,4,1],1) should be 3', function() {
    var result = arraysAnswers.count([ 1, 2, 4, 4, 3, 4, 3 ], 4);

    expect(result).to.eql(3);
  });

  it('#duplicates(arr) you should be able to find duplicates in an array. Example:  duplicates([1,2,4,4,3,3,1,5,3]) should be [1,3,4]', function() {
    var result = arraysAnswers.duplicates([ 1, 2, 4, 4, 3, 3, 1, 5, 3 ]);

    expect(result).to.have.length(3);
    expect(result.sort().join(' ')).to.eql('1 3 4');
  });

  it('#square(arr) you should be able to square each number in an array.Example: square([1,2,3,4]) should be [1,4,6,16]', function() {
    var result = arraysAnswers.square(a);

    expect(result).to.have.length(4);
    expect(result.join(' ')).to.eql('1 4 9 16');
  });

  it('#findAllOccurrences(arr, item) you should be able to find all occurrences of an item in an array. Example: findAllOccurrences([a,b,c,d,e,f,a,b,c] , a) should be [0,6]', function() {
    var result = arraysAnswers.findAllOccurrences('abcdefabc'.split(''), 'a');

    expect(result.sort().join(' ')).to.eql('0 6');
  });

});
